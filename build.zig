const std = @import("std");

pub fn build(b: *std.Build) anyerror!void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const code_bits = b.option(
        u16,
        "code_bits",
        "Number of bits for code type (default: 9)",
    ) orelse 9;

    const symbol_bits = b.option(
        u16,
        "symbol_bits",
        "Number of bits for symbol type (default: 8)",
    ) orelse 8;

    if (code_bits <= symbol_bits) {
        return error.CodeBitsNotGreaterThenSymbolBits;
    }

    const options = b.addOptions();

    options.addOption(u16, "code_bits", code_bits);
    options.addOption(u16, "symbol_bits", symbol_bits);

    const exe = b.addExecutable(.{
        .name = "LZigW",
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });
    exe.addOptions("build_options", options);
    b.installArtifact(exe);

    const run_cmd = b.addRunArtifact(exe);
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    const unit_tests = b.addTest(.{
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });
    unit_tests.addOptions("build_options", options);

    const run_unit_tests = b.addRunArtifact(unit_tests);

    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&run_unit_tests.step);
}
