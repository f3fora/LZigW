const std = @import("std");
const Allocator = std.mem.Allocator;
const ArrayList = std.ArrayList;
const maxInt = std.math.maxInt;

fn typeMax(
    comptime AType: type,
    comptime BType: type,
) BType {
    return @as(BType, maxInt(AType));
}

pub fn String(
    comptime SymbolType: type,
    comptime CodeType: type,
) type {
    return struct {
        prefix: ?CodeType = null,
        extension: SymbolType,
    };
}

const LZWErrors = error{
    OutOfSpace,
    NotInDictionary,
};

pub fn Dictionary(comptime SymbolType: type, comptime CodeType: type) type {
    return ArrayList(String(SymbolType, CodeType));
}

//
// Init a dictionary with the elements present in the string
//
pub fn dictionaryInit(
    comptime SymbolType: type,
    comptime CodeType: type,
    allocator: Allocator,
) anyerror!Dictionary(SymbolType, CodeType) {
    var dict = Dictionary(SymbolType, CodeType).init(allocator);
    var elem: SymbolType = 0;
    while (elem < typeMax(SymbolType, SymbolType)) : (elem += 1) {
        try dict.append(.{ .extension = elem });
    }
    try dict.append(.{ .extension = typeMax(SymbolType, SymbolType) });
    return dict;
}

//
// Check if an element is present in Dictionary and eventually return its representation
//
pub fn dictionaryIsPresentEncode(
    comptime SymbolType: type,
    comptime CodeType: type,
    dict: Dictionary(SymbolType, CodeType),
    elem: String(SymbolType, CodeType),
) LZWErrors!?CodeType {
    if (elem.prefix) |e| {
        const start_index = @max(typeMax(SymbolType, CodeType) + 1, e);
        for (dict.items[start_index..], 0..) |u_elem, i| {
            if ((e == u_elem.prefix.?) and (u_elem.extension == elem.extension)) {
                const pos = start_index + i;
                if (pos >= typeMax(CodeType, usize)) {
                    return LZWErrors.OutOfSpace;
                }
                return @intCast(pos);
            }
        }
    }
    return null;
}

pub fn dictionaryString(
    comptime SymbolType: type,
    comptime CodeType: type,
    dict: Dictionary(SymbolType, CodeType),
    index: CodeType,
) LZWErrors!String(SymbolType, CodeType) {
    if (index >= dict.items.len) {
        return LZWErrors.NotInDictionary;
    }
    return dict.items[@as(usize, index)];
}

pub fn dictionaryIsPresentDecode(
    comptime SymbolType: type,
    comptime CodeType: type,
    dict: Dictionary(SymbolType, CodeType),
    index: CodeType,
) anyerror!?String(SymbolType, CodeType) {
    if (dict.items.len > index) {
        return try dictionaryString(SymbolType, CodeType, dict, index);
    }
    return null;
}

test "dictionaryInit" {
    const test_allocator = std.testing.allocator;
    const expect = std.testing.expect;

    const CodeType = u12;
    const SymbolType = u8;

    var dict = try dictionaryInit(
        SymbolType,
        CodeType,
        test_allocator,
    );
    defer dict.deinit();

    try expect(dict.items.len == typeMax(SymbolType, CodeType) + 1);
}

test "dictionaryIsPresentEncode" {
    const test_allocator = std.testing.allocator;
    const expect = std.testing.expect;

    const CodeType = u12;
    const SymbolType = u8;

    var dict = try dictionaryInit(
        SymbolType,
        CodeType,
        test_allocator,
    );
    defer dict.deinit();
    const string = String(SymbolType, CodeType){ .prefix = 2, .extension = 1 };
    try dict.append(string);

    try expect((try dictionaryIsPresentEncode(SymbolType, CodeType, dict, .{ .extension = 1, .prefix = 1 })) == null);
    try expect((try dictionaryIsPresentEncode(SymbolType, CodeType, dict, string)).? == @as(CodeType, @intCast(dict.items.len - 1)));
}

test "dictionaryString" {
    const test_allocator = std.testing.allocator;
    const expect = std.testing.expect;

    const CodeType = u12;
    const SymbolType = u8;

    var dict = try dictionaryInit(
        SymbolType,
        CodeType,
        test_allocator,
    );
    defer dict.deinit();

    const string = String(SymbolType, CodeType){ .prefix = 2, .extension = 1 };
    try dict.append(string);

    try expect((try dictionaryString(SymbolType, CodeType, dict, 1)).prefix == null);
    try expect((try dictionaryString(SymbolType, CodeType, dict, 1)).extension == 1);

    try expect((try dictionaryString(SymbolType, CodeType, dict, @as(CodeType, @intCast(dict.items.len - 1)))).prefix.? == string.prefix.?);
    try expect((try dictionaryString(SymbolType, CodeType, dict, @as(CodeType, @intCast(dict.items.len - 1)))).extension == string.extension);
}

test "dictionaryIsPresentDecode" {
    const test_allocator = std.testing.allocator;
    const expect = std.testing.expect;

    const CodeType = u12;
    const SymbolType = u8;

    var dict = try dictionaryInit(
        SymbolType,
        CodeType,
        test_allocator,
    );
    defer dict.deinit();

    const string = String(SymbolType, CodeType){ .prefix = 2, .extension = 1 };
    try dict.append(string);

    try expect((try dictionaryIsPresentDecode(SymbolType, CodeType, dict, @as(CodeType, @intCast(dict.items.len - 1)))).?.prefix.? == string.prefix.?);
    try expect((try dictionaryIsPresentDecode(SymbolType, CodeType, dict, @as(CodeType, @intCast(dict.items.len - 1)))).?.extension == string.extension);

    try expect((try dictionaryIsPresentDecode(SymbolType, CodeType, dict, @as(CodeType, @intCast(dict.items.len)))) == null);
}
