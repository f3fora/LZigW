const std = @import("std");
const Allocator = std.mem.Allocator;

const dictionary = @import("dictionary.zig");
const Dictionary = dictionary.Dictionary;
const dictionaryInit = dictionary.dictionaryInit;
const dictionaryIsPresent = dictionary.dictionaryIsPresentEncode;
const String = dictionary.String;

//
// LZW encoding algorithm
//
pub fn encode(
    comptime SymbolType: type,
    comptime CodeType: type,
    allocator: Allocator,
    symbol_stream: anytype,
    code_stream: anytype,
) anyerror!void {
    const code_size = @bitSizeOf(CodeType);
    const symbol_size = @bitSizeOf(SymbolType);

    var dict = try dictionaryInit(
        SymbolType,
        CodeType,
        allocator,
    );
    defer dict.deinit();

    var out_bits: usize = undefined;
    var symbol = try symbol_stream.readBits(SymbolType, symbol_size, &out_bits);
    var current_string: String(SymbolType, CodeType) = .{
        // key part of the algorithm
        .prefix = symbol,
        // for initialization
        .extension = undefined,
    };

    step: while (true) {
        current_string.extension = try symbol_stream.readBits(SymbolType, symbol_size, &out_bits);
        if (out_bits <= 0) {
            try code_stream.writeBits(@as(CodeType, current_string.prefix.?), code_size);
            try code_stream.flushBits();
            break :step;
        }
        const isPresent = try dictionaryIsPresent(SymbolType, CodeType, dict, current_string);
        if (isPresent) |code| {
            current_string.prefix = code;
        } else {
            try code_stream.writeBits(@as(CodeType, current_string.prefix.?), code_size);
            try dict.append(current_string);
            current_string.prefix = current_string.extension;
        }
    }
}

test "encode" {
    const test_allocator = std.testing.allocator;
    const expect = std.testing.expect;
    const fixedBufferStream = std.io.fixedBufferStream;

    const bitReader = std.io.bitReader;
    const bitWriter = std.io.bitWriter;

    const CodeType = u12;
    const SymbolType = u8;
    const code_size = @bitSizeOf(CodeType);

    var in = [_]u8{ 'A', 'B', 'C' } ** 6;

    // For SymbolType at 8 bits
    const exp_vals = [_]CodeType{ 65, 66, 67, 255 + 1, 255 + 3, 255 + 2, 255 + 4, 255 + 7, 255 + 2 }; // ABCDFEGJE

    var out = [_]u8{0} ** 30;
    var exp = [_]u8{0} ** 30;

    var mem_in_be = fixedBufferStream(&in);
    var stream_in = bitReader(.Big, mem_in_be.reader());

    var mem_out_be = fixedBufferStream(&out);
    var stream_out = bitWriter(.Big, mem_out_be.writer());

    var mem_exp_be = fixedBufferStream(&exp);
    var stream_exp = bitWriter(.Big, mem_exp_be.writer());

    for (exp_vals) |symbol| {
        try stream_exp.writeBits(@as(CodeType, symbol), code_size);
    }
    try stream_exp.flushBits();

    try encode(SymbolType, CodeType, test_allocator, &stream_in, &stream_out);

    for (exp, 0..) |exp_v, i| {
        try expect(out[i] == exp_v);
    }
}
