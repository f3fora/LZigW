const std = @import("std");
const ArenaAllocator = std.heap.ArenaAllocator;
const cmp = std.cstr.cmp;
const File = std.fs.File;
const span = std.mem.span;

const bitReader = std.io.bitReader;
const bitWriter = std.io.bitWriter;

const decode = @import("decode.zig").decode;
const encode = @import("encode.zig").encode;
const build_options = @import("build_options");

const unsigned = std.builtin.Signedness.unsigned;
const Int = std.meta.Int;

const CodeType = Int(unsigned, build_options.code_bits);
const SymbolType = Int(unsigned, build_options.symbol_bits);

const Args = enum {
    help,
    h,
    decode,
    d,
    encode,
    e,
    quiet,
    q,
    output,
    o,
};

const Action = enum {
    decode,
    encode,
};

fn stringToArgs(str: []const u8) ?Args {
    if (str[0] != '-') return null;
    const aus = if (str[1] != '-') str[1..] else str[2..];
    return std.meta.stringToEnum(Args, aus);
}

pub fn main() anyerror!void {
    const argv: [][*:0]const u8 = std.os.argv;

    var quiet = false;
    var action: ?Action = null;
    var in_path: ?[:0]const u8 = null;
    var out_path: ?[:0]const u8 = null;

    {
        var look_for_in_path = false;
        var look_for_out_path = false;
        for (argv[1..]) |this_argv| {
            const arg = span(this_argv);
            if (stringToArgs(arg)) |do| switch (do) {
                .h, .help => {
                    try helpMsg();
                    return;
                },
                .q, .quiet => {
                    quiet = true;
                },
                .d, .decode => {
                    if (action) |_| {
                        try helpErr();
                        return;
                    } else action = .decode;
                    look_for_in_path = true;
                    look_for_out_path = false;
                },
                .e, .encode => {
                    if (action) |_| {
                        try helpErr();
                        return;
                    } else action = .encode;
                    look_for_in_path = true;
                    look_for_out_path = false;
                },
                .o, .output => {
                    look_for_in_path = false;
                    look_for_out_path = true;
                },
            } else if (look_for_in_path) {
                in_path = arg;
                look_for_in_path = false;
                look_for_out_path = false;
            } else if (look_for_out_path) {
                out_path = arg;
                look_for_in_path = false;
                look_for_out_path = false;
            } else {
                try helpErr();
                return;
            }
        }
    }
    if (action == null) {
        try helpErr();
        return;
    }

    var arena = ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    var in_file: File = undefined;
    if (in_path) |path| {
        in_file = try std.fs.cwd().openFile(path, .{ .mode = .read_only });
    } else {
        in_file = std.io.getStdIn();
    }
    defer in_file.close();

    var out_file: File = undefined;
    if (out_path) |path| {
        // BREAKING-CHANGE in zig 0.10.0
        out_file = try std.fs.cwd().createFile(path, .{});
    } else {
        out_file = std.io.getStdOut();
    }
    defer out_file.close();

    var in_stream = bitReader(.Big, in_file.reader());
    var out_stream = bitWriter(.Big, out_file.writer());

    switch (action.?) {
        .decode => {
            try decodeMsg(quiet);
            decode(SymbolType, CodeType, allocator, &in_stream, &out_stream) catch |err| {
                try decodeErr(quiet);
                return err;
            };
        },
        .encode => {
            try encodeMsg(quiet);
            encode(SymbolType, CodeType, allocator, &in_stream, &out_stream) catch |err| {
                try encodeErr(quiet);
                return err;
            };
        },
    }
}

fn stderrPrint(comptime str: []const u8, args: anytype) anyerror!void {
    const getStdErr = std.io.getStdErr();
    defer getStdErr.close();
    const stderr = getStdErr.writer();
    try stderr.print(str, args);
}

fn helpMsg() anyerror!void {
    const help_msg_str =
        \\Usage: LZigW [OPTION]...
        \\Encode and decode a FILE using LZW alghorithm. 
        \\
        \\One and only one of the following two options is required.
        \\With no FILE, or when FILE is -, read standard input
        \\  -d, --decode [FILE]  compression of a FILE
        \\  -e, --encode [FILE]  decompression of a FILE
        \\
        \\Without this option, write standard output
        \\With no FILE, or when FILE is -, write standard output
        \\  -o, --output [FILE]  write to a FILE
        \\
        \\  -h, --help           display this help and exit
        \\
        \\Compiled for
        \\- Code: {} bits
        \\- Symbol: {} bits
        \\
        \\To change the bits length, recompile with
        \\zig build -Dcode_bits=<int> -Dsymbol_bits=<int>
        \\
    ;
    try stderrPrint(help_msg_str, .{ build_options.code_bits, build_options.symbol_bits });
}

fn helpErr() anyerror!void {
    const help_err_str =
        \\LZigW: invalid option
        \\Try 'LZigW --help' for more information.
        \\
    ;
    try stderrPrint(help_err_str, .{});
}

fn decodeMsg(quiet: bool) anyerror!void {
    if (quiet) {
        return;
    }
    const decode_msg_str =
        \\LZigW: decompression
        \\Code: {} bits -> Symbol: {} bits
        \\
    ;
    try stderrPrint(decode_msg_str, .{ build_options.code_bits, build_options.symbol_bits });
}

fn encodeMsg(quiet: bool) anyerror!void {
    if (quiet) {
        return;
    }
    const encode_msg_str =
        \\LZigW: compression
        \\Symbol: {} bits -> Code: {} bits
        \\
    ;
    try stderrPrint(encode_msg_str, .{ build_options.symbol_bits, build_options.code_bits });
}

fn decodeErr(quiet: bool) anyerror!void {
    if (quiet) {
        return;
    }
    const decode_err_str =
        \\
        \\LZigW: decompression error
        \\
    ;
    try stderrPrint(decode_err_str, .{});
}

fn encodeErr(quiet: bool) anyerror!void {
    if (quiet) {
        return;
    }
    const encode_err_str =
        \\
        \\LZigW: compression error
        \\
    ;
    try stderrPrint(encode_err_str, .{});
}

test "main" {
    _ = @import("encode.zig");
    _ = @import("decode.zig");
    _ = @import("dictionary.zig");
}
