const std = @import("std");
const Allocator = std.mem.Allocator;
const maxInt = std.math.maxInt;

const dictionary = @import("dictionary.zig");
const Dictionary = dictionary.Dictionary;
const dictionaryInit = dictionary.dictionaryInit;
const dictionaryIsPresent = dictionary.dictionaryIsPresentDecode;
const dictionaryString = dictionary.dictionaryString;
const String = dictionary.String;

pub fn decode(
    comptime SymbolType: type,
    comptime CodeType: type,
    allocator: Allocator,
    code_stream: anytype,
    symbol_stream: anytype,
) anyerror!void {
    const code_size = @bitSizeOf(CodeType);
    const symbol_size = @bitSizeOf(SymbolType);

    var dict = try dictionaryInit(
        SymbolType,
        CodeType,
        allocator,
    );
    defer dict.deinit();

    var out_bits: usize = undefined;
    var code = try code_stream.readBits(CodeType, code_size, &out_bits);
    var old_code = code;

    var symbol = (try dictionaryString(
        SymbolType,
        CodeType,
        dict,
        code,
    )).extension;
    try symbol_stream.writeBits(@as(SymbolType, symbol), symbol_size);

    nextCode: while (true) {
        code = try code_stream.readBits(CodeType, code_size, &out_bits);
        if (out_bits < code_size) {
            // Do not flush here
            break :nextCode;
        }
        const isPresent = try dictionaryIsPresent(SymbolType, CodeType, dict, code);
        if (isPresent) |string| {
            symbol = try nextSymbol(
                SymbolType,
                CodeType,
                dict,
                string,
                symbol_stream,
            );
        } else {
            const tmp_symbol = symbol;
            symbol = try nextSymbol(
                SymbolType,
                CodeType,
                dict,
                try dictionaryString(SymbolType, CodeType, dict, old_code),
                symbol_stream,
            );
            try symbol_stream.writeBits(@as(SymbolType, tmp_symbol), symbol_size);
        }

        try dict.append(.{ .prefix = old_code, .extension = symbol });
        old_code = code;
    }
}

fn nextSymbol(
    comptime SymbolType: type,
    comptime CodeType: type,
    dict: Dictionary(SymbolType, CodeType),
    string: String(SymbolType, CodeType),
    symbol_stream: anytype,
) anyerror!SymbolType {
    const symbol_size = @bitSizeOf(SymbolType);

    var symbol: SymbolType = undefined;
    if (string.prefix) |prefix| {
        symbol = try nextSymbol(
            SymbolType,
            CodeType,
            dict,
            try dictionaryString(SymbolType, CodeType, dict, prefix),
            symbol_stream,
        );
    } else {
        symbol = @as(SymbolType, string.extension);
    }

    const char: SymbolType = @as(SymbolType, string.extension);
    try symbol_stream.writeBits(char, symbol_size);
    return symbol;
}

test "nextSymbol" {
    const test_allocator = std.testing.allocator;
    const expect = std.testing.expect;
    const fixedBufferStream = std.io.fixedBufferStream;

    const bitWriter = std.io.bitWriter;

    const CodeType = u12;
    const SymbolType = u8;

    var dict = try dictionaryInit(
        SymbolType,
        CodeType,
        test_allocator,
    );
    defer dict.deinit();

    const exp = [_]u8{ 1, 2, 3, 4 };

    try dict.append(.{ .prefix = 1, .extension = exp[1] });
    try dict.append(.{ .prefix = @as(CodeType, @intCast(dict.items.len - 1)), .extension = exp[2] });

    var out = [_]u8{0} ** 30;
    var mem_out_be = fixedBufferStream(&out);
    var stream_out = bitWriter(.Big, mem_out_be.writer());

    const symbol = try nextSymbol(
        SymbolType,
        CodeType,
        dict,
        .{ .prefix = @as(CodeType, @intCast(dict.items.len - 1)), .extension = exp[3] },
        &stream_out,
    );
    try expect(symbol == exp[0]);
    for (exp, 0..) |exp_v, i| {
        try expect(exp_v == out[i]);
    }
}

test "decode" {
    const test_allocator = std.testing.allocator;
    const expect = std.testing.expect;
    const fixedBufferStream = std.io.fixedBufferStream;

    const bitReader = std.io.bitReader;
    const bitWriter = std.io.bitWriter;

    const CodeType = u12;
    const SymbolType = u8;
    const code_size = @bitSizeOf(CodeType);

    var in = [_]u8{ 'A', 'B', 'C' } ** 6;

    // For SymbolType at 8 bits
    const exp_vals = [_]CodeType{ 65, 66, 67, 255 + 1, 255 + 3, 255 + 2, 255 + 4, 255 + 7, 255 + 2 }; // ABCDFEGJE

    var out = [_]u8{0} ** 30;
    var exp = [_]u8{0} ** 30;

    var mem_in_be = fixedBufferStream(&exp);
    var stream_in = bitReader(.Big, mem_in_be.reader());

    var mem_out_be = fixedBufferStream(&out);
    var stream_out = bitWriter(.Big, mem_out_be.writer());

    var mem_exp_be = fixedBufferStream(&exp);
    var stream_exp = bitWriter(.Big, mem_exp_be.writer());

    for (exp_vals) |symbol| {
        try stream_exp.writeBits(@as(CodeType, symbol), code_size);
    }
    try stream_exp.flushBits();

    try decode(SymbolType, CodeType, test_allocator, &stream_in, &stream_out);

    for (in, 0..) |in_v, i| {
        try expect(out[i] == in_v);
    }
}
