# LZigW

A [Zig](https://ziglang.org/) implementation of Lempel-Ziv-Welch compression algorithm.

## Building

To compile LZigW, ensure that the following packages are installed:

- zig 0.11

Then run:

```sh
zig build [OPTION]...
```

The Project-Specific Options can be listed with:

```sh
zig build --help | grep  -Pzo "Project-Specific Options.*(\n|.)*\n\n"
```

For example, to change the bits length, recompile with

```sh
zig build -Dcode_bits=<int> -Dsymbol_bits=<int>
```

The executable file can be found in `zig-out/bin/` with the name `LZigW`.

## Usage

Run:

```sh
LZigW [OPTION]...
```

To get the full documentation:

```sh
LZigW --help
```

## References

Welch, "A Technique for High-Performance Data Compression," in Computer, vol. 17, no. 6, pp. 8-19, June 1984, doi: 10.1109/MC.1984.1659158.
